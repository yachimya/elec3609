# LectureTalk

# Installing Guide (ideally running on Linux)
1. Install npm "sudo apt-get install npm"
2. Install Node.js "sudo apt-get install nodejs"
3. Install git "sudo apt-get install git-core"
4. Clone the repository "git clone https://<UNIKEY>@bitbucket.org/<UNIKEY>/elec3609.git"
5. Install sails.js "sudo npm install sails -g"
6. Install nodemailer "sudo npm install nodemailer -g"
7. Install nodemon "sudo npm install nodemon -g"
7. Install nodemon "sudo npm install nodemon -g"
8. Install node-cron "sudo npm install cron"
9. Install other packages "sudo npm install"
10. Start the server by browsing the corresponding directory and run "nodemon app.js"

a [Sails](http://sailsjs.org) application